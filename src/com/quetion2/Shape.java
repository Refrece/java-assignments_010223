package com.quetion2;

public abstract class Shape {
	public abstract void RectangleArea(int len, int breth);

	public abstract void SquareArea(int side);

	public abstract void CircleArea(double radius);
}

package com.quetion2;

public class Area extends Shape {
	 @Override
	    public void RectangleArea(int len, int breth) {
	        System.out.println("RectangleArea value is :"+len*breth);
	    }

	    @Override
	    public void SquareArea(int side) {
	        System.out.println("SquareArea value is :"+side*side);
	    }

	    @Override
	    public void CircleArea(double radius) {
	        System.out.println("radius value is :"+radius*radius%3.14);
	    }
	    public static void main(String[] args) {
	        Area a=new Area();
	        a.RectangleArea(20, 20);
	        a.SquareArea(20);
	        a.CircleArea(80);
	        
	    }
}
